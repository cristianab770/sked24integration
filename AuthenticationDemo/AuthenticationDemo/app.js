﻿var config = {
    // the app id you get after registering your new application using the 
    // new app registration portal: https://apps.dev.microsoft.com
    clientId: "",
    authorizationEndpoint: "https://login.microsoftonline.com/common/oauth2/v2.0/authorize",
    outlookCalendarScopes: ["https://outlook.office.com/calendars.readwrite"],
    redirectUri: "http://localhost:2075",
    apiEndpoint: "http://localhost:55353/api/authentication"
};

window.onload = function () {
    // if the authentication process was succesful, the user will be redirected to the url 
    // specified in the redirect URI param sent in the request with a authorization code
    // in the query string 
    var code = getQueryStringParam("code");
    if (code) {
        postToWebApi(config.apiEndpoint, code);
    }
}


function connectOffice365Account() {
    var authorizationUrl = createNavigateUrl(config.authorizationEndpoint, config.clientId,
        config.outlookCalendarScopes, "code", config.redirectUri, "query") + "&prompt=select_account";
    promptUser(authorizationUrl);
}

// creates the URL for the authentication request with all the required data
// - make sure "offline_access" is added to the scope in order to recieve a refresh token
// - if you want to receive an id token, make sure open id is included as well in the scopes
// - for the OAuth 2.0 Authorization Code Flow make sure the response type is set to "code"
// - make sure the redirect-uri is registered in the application you configured https://www.screencast.com/t/v9AlQnjXdYau
// you can add an additional state parameter and check if the state of the request is the same 
// with the one of the response
function createNavigateUrl(authorizationEndpoint, clientId, scopes, responseType, redirectUri, responseMode) {
    var queryStringParts = [];
    queryStringParts.push("response_type=" + responseType);
    queryStringParts.push("scope=" + encodeURIComponent("openid offline_access " + parseScope(scopes)));
    queryStringParts.push("client_id=" + encodeURIComponent(clientId));
    queryStringParts.push("redirect_uri=" + encodeURIComponent(redirectUri));
    // queryStringParts.push("state=" + encodeURIComponent(state));
    queryStringParts.push("response_mode" + encodeURIComponent(responseMode));

    if (authorizationEndpoint.indexOf("?") < 0) {
        authorizationEndpoint += "?";
    } else {
        authorizationEndpoint += "&";
    }
    return (authorizationEndpoint + queryStringParts.join("&"));
}

function postToWebApi(endpoint, payload) {
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    var options = {
        method: "POST",
        headers: headers,
        mode: 'cors',
        body: JSON.stringify(payload)
    };

    fetch(endpoint, options)
        .then(function (response) {
            //TODO: let the user know that the connection was successful
        })
        .catch(function (error) {
            console.log(error);
        });
}

function promptUser(urlNavigate) {
    window.location.replace(urlNavigate);
}

function parseScope(scopes) {
    let scopeList = "";
    if (scopes) {
        for (let i = 0; i < scopes.length; ++i) {
            scopeList += (i !== scopes.length - 1) ? scopes[i] + " " : scopes[i];
        }
    }
    return scopeList;
}
function getQueryStringParam(paramName) {
    var qs = (function (queryString) {
        if (queryString == "") return {};
        var queryStringParts = {};
        for (var i = 0; i < queryString.length; ++i) {
            var queryStringFragment = queryString[i].split('=', 2);
            if (queryStringFragment.length == 1)
                queryStringParts[queryStringFragment[0]] = "";
            else
                queryStringParts[queryStringFragment[0]] = decodeURIComponent(queryStringFragment[1].replace(/\+/g, " "));
        }
        return queryStringParts;
    })(window.location.search.substr(1).split('&'));
    if (qs !== undefined) {
        return qs[paramName];
    }
    return null;
}