﻿namespace DummyAPI.Utils
{
    public class Constants
    {
        public const string AUTHORIZATION_CODE_GRANT_TYPE = "authorization_code";
        public const string OFFICE365_NOTIFICATIONS_ODATA_TYPE = "#Microsoft.OutlookServices.PushSubscription";
    }
}