﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DummyAPI.Utils
{
    public class CallServiceHelper
    {
        public static async Task<T> PostAsFormDataAsync<T>(string url, IEnumerable<KeyValuePair<string, string>> postObject, params KeyValuePair<string, string>[] headers)
        {
            using (var client = CreateHttpClient(headers))
            {
                client.DefaultRequestHeaders.Add("ContentType", "application/x-www-form-urlencoded");
                var message = await client.PostAsync(url, new FormUrlEncodedContent(postObject));
                if (!message.IsSuccessStatusCode)
                {
                    var errorMessage = await message.Content.ReadAsStringAsync();
                    var exceptionMessage = new StringBuilder();
                    exceptionMessage.AppendLine($"Error during POST request at URL:{url} with error {errorMessage}");
                    throw new HttpException((int)message.StatusCode, exceptionMessage.ToString());
                }
                return await message.Content.ReadAsAsync<T>();
            }
        }

        public static async Task<T> PostAsJsonAsync<T, TU>(string url, TU postObject, params KeyValuePair<string, string>[] headers)
        {
            using (var client = CreateHttpClient(headers))
            {
                var message = await client.PostAsJsonAsync(url, postObject);
                if (!message.IsSuccessStatusCode)
                {
                    var errorMessage = await message.Content.ReadAsStringAsync();
                    var exceptionMessage = new StringBuilder();
                    exceptionMessage.AppendLine($"Error during POST request at URL:{url} with error {errorMessage}");
                    throw new HttpException((int)message.StatusCode, exceptionMessage.ToString());
                }
                return await message.Content.ReadAsAsync<T>();
            }
        }


        public static HttpClient CreateHttpClient(params KeyValuePair<string, string>[] headers)
        {
            var response = new HttpClient();
            if (headers != null && headers.Any())
            {
                foreach (var header in headers)
                {
                    response.DefaultRequestHeaders.Add(header.Key, header.Value);
                }
            }
            return response;
        }
    }
}