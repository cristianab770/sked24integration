﻿using System;

namespace DummyAPI.Models
{
    public class WebhookSubscriptionResponse
    {
        public string Id { get; set; }
        public DateTime SubscriptionExpirationDateTime { get; set; }
    }
}