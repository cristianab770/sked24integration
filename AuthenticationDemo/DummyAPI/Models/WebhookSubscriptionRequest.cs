﻿using System.Runtime.Serialization;

namespace DummyAPI.Models
{
    [DataContract]
    public class WebhookSubscriptionRequest
    {
        [DataMember(Name = "@odata.type")]
        public string OdataType { get; set; }
        [DataMember]
        public string Resource { get; set; }
        [DataMember]
        public string NotificationURL { get; set; }
        [DataMember]
        public string ChangeType { get; set; }
    }

    public enum EventChangeType
    {
        Created
    }
}