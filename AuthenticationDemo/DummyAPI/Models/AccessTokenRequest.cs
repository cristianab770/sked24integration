﻿using System.Runtime.Serialization;

namespace DummyAPI.Models
{
    [DataContract]
    public class GetAccessTokenRequest : AccessTokenRequestBase
    {
        [DataMember(Name = "code")]
        public string Code { get; set; }
    }

    [DataContract]
    public class AccessTokenRequestBase
    {
        [DataMember(Name = "client_id")]
        public string ClientId { get; set; }
        [DataMember(Name = "scope")]
        public string Scope { get; set; }
        [DataMember(Name = "grant_type")]
        public string GrantType { get; set; }
        [DataMember(Name = "redirect_uri")]
        public string RedirectUri { get; set; }
        [DataMember(Name = "client_secret")]
        public string ClientSecret { get; set; }
    }
}