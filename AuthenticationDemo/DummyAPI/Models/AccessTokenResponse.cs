﻿using System;
using System.Runtime.Serialization;

namespace DummyAPI.Models
{
    internal class TokenResponseClaim
    {
        public const string Code = "code";
        public const string TokenType = "token_type";
        public const string AccessToken = "access_token";
        public const string RefreshToken = "refresh_token";
        public const string Scope = "scope";
        public const string ClientInfo = "client_info";
        public const string IdToken = "id_token";
        public const string ExpiresIn = "expires_in";
        public const string Claims = "claims";
        public const string Error = "error";
        public const string ErrorDescription = "error_description";
        public const string ErrorCodes = "error_codes";
        public const string CorrelationId = "correlation_id";
    }
    [DataContract]
    public class AccessTokenResponse
    {
        [DataMember(Name = TokenResponseClaim.TokenType, IsRequired = false)]
        public string TokenType { get; set; }

        [DataMember(Name = TokenResponseClaim.AccessToken, IsRequired = false)]
        public string AccessToken { get; set; }

        [DataMember(Name = TokenResponseClaim.RefreshToken, IsRequired = false)]
        public string RefreshToken { get; set; }

        [DataMember(Name = TokenResponseClaim.Scope, IsRequired = false)]
        public string Scope { get; set; }

        [DataMember(Name = TokenResponseClaim.ClientInfo, IsRequired = false)]
        public string ClientInfo { get; set; }

        [DataMember(Name = TokenResponseClaim.IdToken, IsRequired = false)]
        public string RawIdToken { get; set; }

        [DataMember(Name = TokenResponseClaim.ExpiresIn, IsRequired = false)]
        public long ExpiresIn { get; set; }

        public DateTimeOffset AccessTokenExpiresOn
        {
            get { return DateTime.UtcNow + TimeSpan.FromSeconds(ExpiresIn); }
        }

        [DataMember(Name = TokenResponseClaim.Error, IsRequired = false)]
        public string Error { get; set; }

        [DataMember(Name = TokenResponseClaim.ErrorDescription, IsRequired = false)]
        public string ErrorDescription { get; set; }

        [DataMember(Name = TokenResponseClaim.ErrorCodes, IsRequired = false)]
        public string[] ErrorCodes { get; set; }

        [DataMember(Name = TokenResponseClaim.CorrelationId, IsRequired = false)]
        public string CorrelationId { get; set; }

        [DataMember(Name = TokenResponseClaim.Claims, IsRequired = false)]
        public string Claims { get; set; }
        public IdToken IdToken { get; set; }


        // This method is called after the object 
        // is completely deserialized.
        [OnDeserialized]
        void OnDeserialized(StreamingContext context)
        {
            IdToken = IdToken.Parse(RawIdToken);
        }
    }
}