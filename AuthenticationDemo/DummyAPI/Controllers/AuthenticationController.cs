﻿using DummyAPI.Models;
using DummyAPI.Utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using System.Web.Http;

namespace DummyAPI.Controllers
{
    public class AuthenticationController : ApiController
    {
        // The Client ID is used by the application to uniquely identify itself to Azure AD.
        string clientId = ConfigurationManager.AppSettings["ClientId"];
        // RedirectUri must be the same one used to get the authorization code
        string redirectUri = ConfigurationManager.AppSettings["RedirectUri"];
        // Tenant is the tenant ID ('common' for multi-tenant)
        static string tenant = ConfigurationManager.AppSettings["Tenant"];
        //  App Secret of the Password type generated in the app registration portal fost the current app
        static string clientSecret = ConfigurationManager.AppSettings["ClientSecret"];
        // Authority is the URL for authority, composed by Azure Active Directory v2 endpoint and the tenant name (e.g. https://login.microsoftonline.com/common/v2.0)
        string authority = String.Format(ConfigurationManager.AppSettings["Authority"], tenant);
        // The required application scopes
        string outlookCalendarScope = "https://outlook.office.com/calendars.readwrite";

        [HttpPost]
        public async Task<IHttpActionResult> AccessToken([FromBody]string code)
        {
            if (!string.IsNullOrWhiteSpace(code))
            {
                var getAccessTokenRequest = new GetAccessTokenRequest()
                {
                    ClientId = clientId,
                    Scope = outlookCalendarScope,
                    RedirectUri = redirectUri,
                    GrantType = Constants.AUTHORIZATION_CODE_GRANT_TYPE,
                    Code = code,
                    ClientSecret = clientSecret
                };
                var getAccessTokenRequestKeyValuePairs = getAccessTokenRequest.ToKeyValue();
                //TODO: Retry policy
                var getAccessTokenResponse = await CallServiceHelper.PostAsFormDataAsync<AccessTokenResponse>(GetTokenEndpoint(tenant), getAccessTokenRequestKeyValuePairs, null);

                // TODO: make sure you save the access token, the refresh token and the tenat ID

                if (getAccessTokenResponse != null
                    && !string.IsNullOrWhiteSpace(getAccessTokenResponse.AccessToken)
                    && getAccessTokenResponse.IdToken != null
                    && !string.IsNullOrWhiteSpace(getAccessTokenResponse.IdToken.TenantId))
                {
                    // Subscribe to Office 365 Notifications in order to get a notification 
                    // (using a webhook), every time a new event is added to the calendar
                    await SubscribeToOffice365CalendarEvents(getAccessTokenResponse.AccessToken, getAccessTokenResponse.IdToken.TenantId);
                }
            }
            return Ok();
        }

        private async Task<WebhookSubscriptionResponse> SubscribeToOffice365CalendarEvents(string accessToken, string tenantId)
        {
            WebhookSubscriptionResponse result = null;
            if (!string.IsNullOrWhiteSpace(accessToken) && !string.IsNullOrWhiteSpace(tenantId))
            {
                var webhookSubscriptionRequest = new WebhookSubscriptionRequest()
                {
                    OdataType = Constants.OFFICE365_NOTIFICATIONS_ODATA_TYPE,
                    Resource = ConfigurationManager.AppSettings["CalendarResourceURL"],
                    NotificationURL = ConfigurationManager.AppSettings["CalendarNotificationURL"],
                    ChangeType = Enum.GetName(typeof(EventChangeType), EventChangeType.Created)
                };
                var authorizationHeader = new KeyValuePair<string, string>("Authorization", $"Bearer {accessToken}");
                //TODO: retry policy
                result = await CallServiceHelper.PostAsJsonAsync<WebhookSubscriptionResponse, WebhookSubscriptionRequest>(ConfigurationManager.AppSettings["Office365SubscriptionEndpoint"],
                    webhookSubscriptionRequest, authorizationHeader);
            }
            return result;
        }

        private string GetTokenEndpoint(string tenant)
        {
            string result = null;
            if (!string.IsNullOrWhiteSpace(tenant))
            {
                string authorityBase = ConfigurationManager.AppSettings["Authority"];
                if (!string.IsNullOrWhiteSpace(authorityBase))
                {
                    result = $"{String.Format(authorityBase, tenant)}/token";
                }
            }
            return result;
        }
    }
}
